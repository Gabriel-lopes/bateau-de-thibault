/* ***********************************************************
 * TIG - Le bateau de Thibault.
 * Copyright (C) 2018 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: BateauThibault/app/src/views/accueil/accueil.js updated 2020-03-03 buixuan.
 * ***********************************************************/
var frameModule = require("ui/frame");

var page;
exports.loaded = function(args) {
  page=args.objects;
};

exports.bateau = function() { frameModule.topmost().navigate("src/views/bateau/bateau"); };
exports.restaurant = function() { frameModule.topmost().navigate("src/views/restaurant/restaurant"); };
exports.recette = function() { frameModule.topmost().navigate("src/views/recette/recette"); };
exports.contact = function() { frameModule.topmost().navigate("src/views/contact/contact"); };
exports.shop = function() { frameModule.topmost().navigate("src/views/shop/shop"); };
