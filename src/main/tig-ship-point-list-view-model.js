/* ***********************************************************
 * TIG - Le bateau de Thibault.
 * Copyright (C) 2018 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: BateauThibault/app/src/main/tig-ship-point-list-view-model.js updated 2020-03-03 buixuan.
 * ***********************************************************/
var config = require("~/src/main/config");
var fetchModule = require("fetch");
var ObservableArray = require("data/observable-array").ObservableArray;

function TIGShipPointListViewModel(items) {
    var viewModel = new ObservableArray(items);
    viewModel.load = function() {
        return fetch(config.apiUrl + "shipPoints/")
        .then(handleErrors)
        .then(function(response) {
            return response.json();
        }).then(function(data) {
            data.forEach(function(article) {
                viewModel.push({
                    id: article.id,
                    name: article.name,
                    address: article.address,
                    date: article.date,
                    selected: false
                });
            });
        });
    };
    viewModel.empty = function() {
        while (viewModel.length) {
            viewModel.pop();
        }
    };

    return viewModel;
}

function handleErrors(response) {
    if (!response.ok) {
        console.log(JSON.stringify(response));
        throw Error(response.statusText);
    }
    return response;
}

module.exports = TIGShipPointListViewModel;
