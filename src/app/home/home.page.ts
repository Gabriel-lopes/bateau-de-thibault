import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ProduitsPage } from '../produits/produits.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private route: Router) {}

  onClickedProduits() {
    this.route.navigate(['/produits']);
  }
}
